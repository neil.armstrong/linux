Neil's Upstream Work Branches
=============================

Branches
--------

topic/`soc`/`status`/`subject` are the work branches, if `status` is `upstream` then
it's a working branch for an ongoing upstream patchset.

Tags
----

sent/topic/`soc`/upstream/`subject`-v`<version>` are the tags containing the patches sent to
the mailing list tagged with the version.

Contact
-------

Contact neil[dot]armstrong[at]linaro.org or OFTC/Libera.Chat as narmstrong on #linux-amlogic, #u-boot or #linux-msm
